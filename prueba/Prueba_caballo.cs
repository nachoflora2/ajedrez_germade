﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ajedrez;
using System;
using System.Collections.Generic;
using System.Text;

namespace prueba
{
    [TestClass()]
    public class CaballoTests
    {

        Caballo caballoB = new Caballo(2, 3, true);
        Caballo caballoN = new Caballo(7, 5, false);

        [TestMethod()]
        public void CaballoTest()
        {

        }

        [TestMethod()]
        public void LizqTest()
        {
            bool test;
            caballoN.Lizq();
            caballoB.Lizq();
            if (caballoB.getY() == 5 && caballoB.getX() == 1 && caballoN.getY() == 3 && caballoB.getX() == 8)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }
        [TestMethod()]
        public void LderTest()
        {
            bool test;
            caballoN.Lder();
            caballoB.Lder();
            if (caballoB.getY() == 5 && caballoB.getX() == 3 && caballoN.getY() == 3 && caballoB.getX() == 6)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }
        [TestMethod()]
        public void LizqatrasTest()
        {
            bool test;
            caballoN.Lizqatras();
            caballoB.Lizqatras();
            if (caballoB.getY() == 1 && caballoB.getX() == 1 && caballoN.getY() == 7 && caballoB.getX() == 8)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }
        [TestMethod()]
        public void LderatrasTest()
        {
            bool test;
            caballoN.Lderatras();
            caballoB.Lderatras();
            if (caballoB.getY() == 1 && caballoB.getX() == 3 && caballoN.getY() == 7 && caballoB.getX() == 6)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }
    }
}

