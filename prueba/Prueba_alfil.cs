﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ajedrez;
using System;
using System.Collections.Generic;
using System.Text;

namespace prueba
{
    [TestClass()]
    public class AlfilTests
    {

        Alfil alfilB = new Alfil(3, 2, true);
        Alfil alfilN = new Alfil(3, 7, false);

        [TestMethod()]
        public void AlfilTest()
        {

        }

        [TestMethod()]
        public void moverizqTest()
        {
            bool test;
            alfilN.moverizq();
            alfilB.moverder();
            if (alfilB.getY() == 3 && alfilB.getX() == 2 && alfilN.getY() == 6 && alfilN.getX() == 4)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }

        [TestMethod()]
        public void moverderTest()
        {
            bool test;
            alfilN.moverder();
            alfilB.moverder();
            if (alfilB.getY() == 3 && alfilB.getX() == 4 && alfilN.getY() == 6 && alfilN.getX() == 2)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }

        [TestMethod()]
        public void moveratrasderTest()
        {
            bool test;
            alfilN.atrasder();
            alfilB.atrasder();
            if (alfilB.getY() == 1 && alfilB.getX() == 4 && alfilN.getY() == 8 && alfilN.getX() == 2)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }

        [TestMethod()]
        public void moveratrasizqTest()
        {
            bool test;
            alfilN.atrasizq();
            alfilB.atrasizq();
            if (alfilB.getY() == 1 && alfilB.getX() == 2 && alfilN.getY() == 8 && alfilN.getX() == 4)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }
    }
}
