﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ajedrez;
using System;
using System.Collections.Generic;
using System.Text;

namespace prueba
{
    [TestClass()]
    public class ReinaTests
    {

        Reina reinaB = new Reina(4, 2, true);
        Reina reinaN = new Reina(4, 7, false);

        [TestMethod()]
        public void ReinaTest()
        {

        }

        [TestMethod()]
        public void moverizqTest()
        {
            bool test;
            reinaN.moverizq();
            reinaB.moverder();
            if (reinaB.getY() == 3 && reinaB.getX() == 3 && reinaN.getY() == 6 && reinaN.getX() == 5)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }

        [TestMethod()]
        public void moverderTest()
        {
            bool test;
            reinaN.moverder();
            reinaB.moverder();
            if (reinaB.getY() == 3 && reinaB.getX() == 5 && reinaN.getY() == 6 && reinaN.getX() == 3)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }

        [TestMethod()]
        public void moveratrasderTest()
        {
            bool test;
            reinaN.atrasder();
            reinaB.atrasder();
            if (reinaB.getY() == 1 && reinaB.getX() == 5 && reinaN.getY() == 8 && reinaN.getX() == 3)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }

        [TestMethod()]
        public void moveratrasizqTest()
        {
            bool test;
            reinaN.atrasizq();
            reinaB.atrasizq();
            if (reinaB.getY() == 1 && reinaB.getX() == 3 && reinaN.getY() == 8 && reinaN.getX() == 5)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }
        [TestMethod()]
        public void AdelanteTest()
        {
            bool test;
            reinaN.adelante();
            reinaB.adelante();
            if (reinaB.getY() == 3 && reinaN.getY() == 6)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }
        [TestMethod()]
        public void atrasTest()
        {
            bool test;
            reinaN.atras();
            reinaB.atras();
            if (reinaB.getY() == 1 && reinaN.getY() == 8)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }
        [TestMethod()]
        public void derechaTest()
        {
            bool test;
            reinaN.derecha();
            reinaB.derecha();
            if (reinaB.getX() == 5 && reinaN.getX() == 3)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }
        [TestMethod()]
        public void izquierdaTest()
        {
            bool test;
            reinaN.izquierda();
            reinaB.izquierda();
            if (reinaB.getX() == 3 && reinaN.getX() == 5)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }
    }
}
