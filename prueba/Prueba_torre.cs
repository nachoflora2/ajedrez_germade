﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ajedrez;
using System;
using System.Collections.Generic;
using System.Text;

namespace prueba
{
    [TestClass()]
    public class TorreTests
    {

        Torre TorreB = new Torre(3, 3, true);
        Torre TorreN = new Torre(3, 7, false);

        [TestMethod()]
        public void TorreTest()
        {

        }

        [TestMethod()]
        public void AdelanteTest()
        {
            bool test;
            TorreN.adelante();
            TorreB.adelante();
            if (TorreB.getY() == 4 && TorreN.getY() == 6)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }
        [TestMethod()]
        public void atrasTest()
        {
            bool test;
            TorreN.atras();
            TorreB.atras();
            if (TorreB.getY() == 2 && TorreN.getY() == 8)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }
        [TestMethod()]
        public void derechaTest()
        {
            bool test;
            TorreN.derecha();
            TorreB.derecha();
            if (TorreB.getX() == 4 && TorreN.getX() == 2)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }
        [TestMethod()]
        public void izquierdaTest()
        {
            bool test;
            TorreN.izquierda();
            TorreB.izquierda();
            if (TorreB.getX() == 2 && TorreN.getX() == 4)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }
    }
}
