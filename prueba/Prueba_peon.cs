using Microsoft.VisualStudio.TestTools.UnitTesting;
using ajedrez;
using System;
using System.Collections.Generic;
using System.Text;

namespace prueba
{
    [TestClass()]
    public class PeonTests
    {

        Peon peonB = new Peon(2, 2, true);
        Peon peonN = new Peon(4, 7, false);

        [TestMethod()]
        public void PeonTest()
        {

        }

        [TestMethod()]
        public void moverTest()
        {
            bool test;
            peonN.mover();
            peonB.mover();
            if (peonB.getY() == 3 && peonN.getY() == 6)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }

        [TestMethod()]
        public void mover2Test()
        {
            bool test;
            peonN.mover2();
            peonB.mover2();
            if (peonB.getY() == 4 && peonN.getY() == 5)
            {
                test = true;
            }
            else
            {
                test = false;
            }

            Assert.AreEqual(test, true);
        }

        [TestMethod()]
        public void comerIzqTest()
        {
            bool test;
            peonN.comerizq();
            peonB.comerizq();
            if (peonB.getY() == 3 && peonN.getY() == 6 && peonB.getX() == 1 && peonN.getX() == 3)
            {
                test = true;
            }
            else
            {
                test = false;
            }

            Assert.AreEqual(test, true);
        }

        [TestMethod()]
        public void comerDerTest()
        {
            bool test;
            peonN.comerder();
            peonB.comerder();
            if (peonB.getY() == 3 && peonN.getY() == 6 && peonB.getX() == 3 && peonN.getX() == 5)
            {
                test = true;
            }
            else
            {
                test = false;
            }

            Assert.AreEqual(test, true);
        }
    }
}
