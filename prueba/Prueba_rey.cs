﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ajedrez;
using System;
using System.Collections.Generic;
using System.Text;

namespace prueba
{
    [TestClass()]
    public class ReyTests
    {

        Rey reyB = new Rey(4, 2, true);
        Rey reyN = new Rey(4, 7, false);

        [TestMethod()]
        public void ReinaTest()
        {

        }

        [TestMethod()]
        public void moverizqTest()
        {
            bool test;
            reyN.moverizq();
            reyB.moverder();
            if (reyB.getY() == 3 && reyB.getX() == 3 && reyN.getY() == 6 && reyN.getX() == 5)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }

        [TestMethod()]
        public void moverderTest()
        {
            bool test;
            reyN.moverder();
            reyB.moverder();
            if (reyB.getY() == 3 && reyB.getX() == 5 && reyN.getY() == 6 && reyN.getX() == 3)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }

        [TestMethod()]
        public void moveratrasderTest()
        {
            bool test;
            reyN.atrasder();
            reyB.atrasder();
            if (reyB.getY() == 1 && reyB.getX() == 5 && reyN.getY() == 8 && reyN.getX() == 3)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }

        [TestMethod()]
        public void moveratrasizqTest()
        {
            bool test;
            reyN.atrasizq();
            reyB.atrasizq();
            if (reyB.getY() == 1 && reyB.getX() == 3 && reyN.getY() == 8 && reyN.getX() == 5)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }
        [TestMethod()]
        public void AdelanteTest()
        {
            bool test;
            reyN.adelante();
            reyB.adelante();
            if (reyB.getY() == 3 && reyN.getY() == 6)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }
        [TestMethod()]
        public void atrasTest()
        {
            bool test;
            reyN.atras();
            reyB.atras();
            if (reyB.getY() == 1 && reyN.getY() == 8)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }
        [TestMethod()]
        public void derechaTest()
        {
            bool test;
            reyN.derecha();
            reyB.derecha();
            if (reyB.getX() == 5 && reyN.getX() == 3)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }
        [TestMethod()]
        public void izquierdaTest()
        {
            bool test;
            reyN.izquierda();
            reyB.izquierda();
            if (reyB.getX() == 3 && reyN.getX() == 5)
            {
                test = false;
            }
            else
            {
                test = true;
            }

            Assert.AreEqual(test, true);
        }
    }
}
