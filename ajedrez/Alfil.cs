﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ajedrez
{
    public class Alfil
    {
        byte x;
        byte y;
        bool color;
        public Alfil(byte x, byte y, bool color)
        {
            this.x = x;
            this.y = y;
            this.color = color;

        }
        public byte getX()
        {
            return x;
        }
        public byte getY()
        {
            return y;
        }
        public void moverizq()
        {
            if (color == true)
            {
                y = y++;
                x = x--;
            }
            else
            {
                y = y--;
                x = x++;
            }
        }
        public void moverder()
        {
            if (color == true)
            {
                y = y++;
                x = x++;
            }
            else
            {
                y = y--;
                x = x--;
            }
        }
        public void atrasizq()
        {
            if (color == true)
            {
                y = y--;
                x = x--;
            }
            else
            {
                y = y++;
                x = x++;
            }
        }
        public void atrasder()
        {
            if (color == true)
            {
                y = y--;
                x = x++;
            }
            else
            {
                y = y++;
                x = x--;
            }
        }
        public void comerizq()
        {
            if (color == true)
            {
                y = y++;
                x = x--;

            }
            else
            {
                y = y--;
                x = x++;
            }
        }
        public void comerder()
        {
            if (color == true)
            {
                y = y++;
                x = x++;

            }
            else
            {
                y = y--;
                x = x--;
            }
        }
        public void comeratrasder()
        {
            if (color == true)
            {
                y = y--;
                x = x++;

            }
            else
            {
                y = y++;
                x = x--;
            }
        }
        public void comeratrasizq()
        {
            if (color == true)
            {
                y = y--;
                x = x--;

            }
            else
            {
                y = y++;
                x = x++;
            }
        }
    }
}
