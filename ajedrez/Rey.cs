﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ajedrez
{
    public class Rey
    {
        byte x;
        byte y;
        bool color;
        public Rey(byte x, byte y, bool color)
        {
            this.x = x;
            this.y = y;
            this.color = color;

        }
        public byte getX()
        {
            return x;
        }
        public byte getY()
        {
            return y;
        }
        public void adelante()
        {
            if (color == true)
            {
                y = y++;
            }
            else
            {
                y = y--;

            }

        }

        public void atras()
        {
            if (color == true)
            {
                y = y--;
            }
            else
            {
                y = y++;

            }

        }
        public void derecha()
        {
            if (color == true)
            {
                x = x++;
            }
            else
            {
                x = x--;

            }

        }
        public void izquierda()
        {
            if (color == true)
            {
                x = x--;
            }
            else
            {
                x = x++;

            }

        }
        public void moverizq()
        {
            if (color == true)
            {
                y = y++;
                x = x--;
            }
            else
            {
                y = y--;
                x = x++;
            }
        }
        public void moverder()
        {
            if (color == true)
            {
                y = y++;
                x = x++;
            }
            else
            {
                y = y--;
                x = x--;
            }
        }
        public void atrasizq()
        {
            if (color == true)
            {
                y = y--;
                x = x--;
            }
            else
            {
                y = y++;
                x = x++;
            }
        }
        public void atrasder()
        {
            if (color == true)
            {
                y = y--;
                x = x++;
            }
            else
            {
                y = y++;
                x = x--;
            }
        }
    }
}
