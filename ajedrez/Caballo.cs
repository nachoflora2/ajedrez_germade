﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ajedrez
{
    public class Caballo
    {
        byte x;
        byte y;
        bool color;
        public Caballo(byte x, byte y, bool color)
        {
            this.x = x;
            this.y = y;
            this.color = color;

        }
        public byte getX()
        {
            return x;
        }
        public byte getY()
        {
            return y;
        }
        public void Lizq()
        {
            if (color == true)
            {
                y += 2;
                x = x--;
            }
            else
            {
                y -= 2;
                x = x++;

            }

        }
        public void Lder()
        {
            if (color == true)
            {
                y += 2;
                x = x++;
            }
            else
            {
                y -= 2;
                x = x--;

            }

        }
        public void Lizqatras()
        {
            if (color == true)
            {
                y -= 2;
                x = x--;
            }
            else
            {
                y += 2;
                x = x++;

            }

        }
        public void Lderatras()
        {
            if (color == true)
            {
                y -= 2;
                x = x++;
            }
            else
            {
                y += 2;
                x = x--;

            }

        }
    }
}
