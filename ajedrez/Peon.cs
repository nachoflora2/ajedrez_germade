﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ajedrez
{
    public class Peon
    {
        byte x;
        byte y;
        bool color;
        public Peon(byte x, byte y, bool color)
        {
            this.x = x;
            this.y = y;
            this.color = color;

        }
        public byte getX()
        {
            return x;
        }
        public byte getY()
        {
            return y;
        }
        public void mover()
        {
            if (color == true)
            {
                y = y++;
            }
            else
            {
                y = y--;
            }
        }
        public void mover2()
        {
            if (color == true)
            {
                y += 2;
            }
            else
            {
                y -= 2;
            }
        }
        public void comerizq()
        {
            if (color == true)
            {
                y++;
                x--;

            }
            else
            {
                y--;
                x--;
            }
        }
        public void comerder()
        {
            if (color == true)
            {
                y++;
                x++;

            }
            else
            {
                y--;
                x++;
            }
        }
    }
}
